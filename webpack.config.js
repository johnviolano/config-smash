"use strict";

const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require("clean-webpack-plugin"); /* Cleans your dist folder */
const { TsConfigPathsPlugin } = require("awesome-typescript-loader"); /* Faster transpiling for TypeScript */

module.exports = {
    plugins: [
        new CopyWebpackPlugin([{from: 'html/index.html' }]),
        new CleanWebpackPlugin(["dist"]), /* as noted above, clean this folder on rebuild */
    ],

    entry: "./src/index.ts", /* this is your "main" */

    module: {
        rules: [
            {
                test: /\.tsx?$/, /* if the filename matches this regex use this loader */
                use: "awesome-typescript-loader",
                exclude: /node_modules/ /* we won't be compiling other peoples typescript */
            }
        ]
    },

    resolve: {
        plugins: [new TsConfigPathsPlugin()], /* when resolving files use this plugin */
        extensions: [".tsx", ".ts", ".js", ".jsx"] /* when you import a file in your code you won't need to add the extension */
    },

    output: {
        path: path.resolve(__dirname, "dist"), /* everything you find in here */
        filename: "[name].bundle.js" /* bundle into this file "main.bundle.js" - if you add more entries above they get their own bundle */
    },

    /* !!!! NON PRODUCTION VALUES !!!! */
    devtool: "inline-source-map", /* adds source map for debugging, this lets you sanely debug in chrome */
    mode: "development"
    /* !!!! NON PRODUCTION VALUES !!!! */
};
