# Setup your project
```
$ npm init -y # create a package.json (your project configuration, including run/test commands)
$ npm install --save-dev typescript # install typescript (type safe JS!), but don't make it a runtime dependency
$ npm install --save-dev webpack webpack-cli webpack-dev-server # install webpack (bundles your project for distribution), but don't make it a runtime dependency
$ npm install --save-dev copy-webpack-plugin clean-webpack-plugin awesome-typescript-loader # install useful little plugins for hotloading your code, but don't make them a runtime dependency

$ tsc --init # create a standard tsconfig.json (this is where your TypeScript options live) - I've added some folder settings and the loader section

$ mkdir src # put .ts files here
$ mkdir dist # webpack will bundle your output here, clean-webpack-plugin will clean this everytime you deploy (see webpack.config.js)
```

I've done three things that were not automatic...
1. Created a `webpack.config.js` for you - this will bundle your javascript and serve as a local webserver for testing. I added a plugin to copy index.html as part of the build process. I assumed you'd be doing some page development, but as you add more pages you'll need to enhance that part of the config.
2. Edited `package.json` for you - I've added 3 lines, build/watch/start. These are commands you can execute with `npm run <build, watch, start>`
3. Edited `tsconfig.json` for you - There was a bit of noodling involved in setting the right paths

# Install VS Code
## https://code.visualstudio.com/download

# Install VS Code Debugger for Chrome
## https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome


# Create .vscode/launch.json in the project's directory
This just tells VS Code how to debug your project
```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost with sourcemaps",
            "url": "http://localhost:8080/index.html",
            "webRoot": "${workspaceFolder}",
            "sourceMaps": true
        }
    ]
}
```

# To just build TypeScript files
```
$ tsc
```

# To run the dev server
Either via the VS Code (recommended) terminal ``(CMD + `)``or a separate one, cd to the project root and
```
$ npm start
```
Note: You can omit "run" for this one special npm command
Note: this will also clean your `dist` folder :)

# To watch files for changes
```
$ npm run watch
```
Note: This will recreate the `dist` folder that just disappeared on you :)

# To debug your code
Press `F5` in VS Code and select Chrome. You can set breakpoints in VS Code and it will hit them.